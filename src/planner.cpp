#include "planner.h"
#include <QDir>
#include <QProcess>

Planner::Planner(QObject *parent) :
    QObject(parent)
{
    mTimer = new QTimer(this);
    mTimer->setInterval(100);
    connect(mTimer, SIGNAL(timeout()), this, SLOT(onTimerTick()));
    mTimer->start();
}

void Planner::addEvent(QString filename, QDateTime time)
{
    if (!mEvents.contains(Event(filename, time))) {
        mEvents.push_back(Event(filename, time));
        emit eventAdded(filename, time);
    }
}

void Planner::removeEvent(QString filename, QDateTime time)
{
    if (mEvents.contains(Event(filename, time))) {
        mEvents.removeOne(Event(filename, time));
        emit eventRemoved(filename, time);
    }
}

QList<Planner::Event> &Planner::getEvents()
{
    return mEvents;
}

void Planner::onTimerTick()
{
    for (Event &e : mEvents) {
        if (e.time() <= QDateTime::currentDateTime()) {
            handleEvent(e);
        }
    }
}

void Planner::handleEvent(Planner::Event &e)
{
    QProcess process;
    process.startDetached(e.filename(), QStringList() << QDir::current().path());
    removeEvent(e.filename(), e.time());
}

QDateTime Planner::Event::time() const
{
	return mTime;
}

void Planner::Event::setTime(const QDateTime &time)
{
	mTime = time;
}

QString Planner::Event::filename() const
{
	return mFilename;
}

void Planner::Event::setFilename(const QString &filename)
{
    mFilename = filename;
}

bool Planner::Event::operator==(const Planner::Event &other)
{
    return this->mTime == other.mTime && this->mFilename == other.mFilename;
}
