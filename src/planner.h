#ifndef PLANNER_H
#define PLANNER_H

#include <QDateTime>
#include <QObject>
#include <QTimer>



class Planner : public QObject
{
	Q_OBJECT
public:
	class Event {
	public:
		Event() {}
        Event(QString fileName, QDateTime time)
            : mFilename(fileName), mTime(time){}

		QDateTime time() const;
		void setTime(const QDateTime &time);
		QString filename() const;
		void setFilename(const QString &filename);

        bool operator==(const Event &other);

	private:
		QString mFilename;
		QDateTime mTime;
	};

    explicit Planner(QObject *parent = nullptr);
	void addEvent(QString filename, QDateTime time);
	void removeEvent(QString filename, QDateTime time);
    QList<Event> &getEvents();

signals:
    void eventAdded(QString name, QDateTime time);
    void eventRemoved(QString name, QDateTime time);

private slots:
    void onTimerTick();

private:
    void handleEvent(Event &e);

    QList<Event> mEvents;
    QTimer *mTimer;
};

#endif // PLANNER_H
