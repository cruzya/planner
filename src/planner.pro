#-------------------------------------------------
#
# Project created by QtCreator 2019-03-19T12:40:54
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = planner
TEMPLATE = app
CONFIG += static

SOURCES += main.cpp\
        mainwindow.cpp \
    planner.cpp

HEADERS  += mainwindow.h \
    planner.h

FORMS    += mainwindow.ui
