#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "planner.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
	~MainWindow();

private slots:
	void on_btnAdd_clicked();
    void updateTime();
    void updateList();

    void on_btnRemove_clicked();

private:
    Planner *mPlanner;

    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
