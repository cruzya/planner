#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QDebug>
#include <QString>
MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);
    QDateTime current = QDateTime::currentDateTime();
    current.setTime(QTime(current.time().hour(), current.time().minute(), current.time().second(), 0));
    ui->dteTime->setDateTime(current);
    mPlanner = new Planner(this);
    connect(mPlanner, SIGNAL(eventAdded(QString, QDateTime)), this, SLOT(updateList()));
    connect(mPlanner, SIGNAL(eventRemoved(QString, QDateTime)), this, SLOT(updateList()));
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateTime()));
    timer->setInterval(100);
    timer->start();
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::on_btnAdd_clicked()
{
    QString filename = QFileDialog::getOpenFileName(this);
    if (filename.isEmpty())
        return;
    mPlanner->addEvent(filename, ui->dteTime->dateTime());
}

void MainWindow::updateTime()
{
    if (!mPlanner->getEvents().empty()) {
        QDateTime min;
        for (Planner::Event &e: mPlanner->getEvents())
            if (min.isNull() || e.time() < min)
                min = e.time();
        QDateTime current = QDateTime::currentDateTime();
        qint64 days = current.daysTo(min);
        if (days == 0) {
            QTime t = QTime::fromMSecsSinceStartOfDay(QDateTime::currentDateTime().time().msecsTo(min.time()));
            ui->lbNextTimer->setText(t.toString());
        }
        else {
            ui->lbNextTimer->setText(QString::number(days) + "days...");
        }
    }
}

void MainWindow::updateList()
{
    ui->listWidget->clear();
    ui->lbNextTimer->clear();
    for (Planner::Event &e: mPlanner->getEvents())
        ui->listWidget->addItem(QString("%1 : %2").arg(e.time().toString("dd.MM.yyyy hh:mm:ss")).arg(e.filename()));
}

void MainWindow::on_btnRemove_clicked()
{
    if (ui->listWidget->currentItem()) {
        QStringList splitted = ui->listWidget->currentItem()->text().split(" : ");
        mPlanner->removeEvent(splitted[1], QDateTime::fromString(splitted[0], "dd.MM.yyyy hh:mm:ss"));
    }
}
